package ru.kolesnikov.tm.api.controller;

public interface ICommandController {

    void showVersion();

    void showAbout();

    void exit();

    void showCommands();

    void showArguments();

    void showHelp();

    void showInfo();

}
