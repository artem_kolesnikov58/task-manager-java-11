package ru.kolesnikov.tm.api.repository;

import ru.kolesnikov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

}
